import { FetchRequest, JsonRpcProvider } from "ethers";
import { config } from "../config.js";
import { Chains, ChainsDataType, ChainsType, Proxy } from "../src/types.js";
import { gotWithProxy, testProxy } from "../src/utils/gotHeadersProxy.js";

export class ProviderManager {
	static #providers: ChainsDataType = {
		[Chains.Ethereum]: {
			provider: new JsonRpcProvider(config.RPCs[Chains.Ethereum]),
			explorer: "https://etherscan.io",
			chainId: 1,
		},
		[Chains.Zora]: {
			provider: new JsonRpcProvider(config.RPCs[Chains.Zora]),
			explorer: "https://explorer.zora.co",
			chainId: 7777777,
		},
	};

	static get ethProvider(): JsonRpcProvider {
		return this.#providers[Chains.Ethereum].provider;
	}

	static get zoraProvider(): JsonRpcProvider {
		return this.#providers[Chains.Zora].provider;
	}

	static getProvider(chain: ChainsType): JsonRpcProvider {
		return this.#providers[chain].provider;
	}

	static getExplorer(chain: ChainsType): string {
		return this.#providers[chain].explorer;
	}

	static getExplorerByChainId(chainId: number): string {
		for (let chain in this.#providers) {
			if (this.#providers[chain as ChainsType].chainId === chainId) {
				return this.#providers[chain as ChainsType].explorer;
			}
		}
		throw new Error("Wrong chain Id, no provider found");
	}

	static updateProxy(proxy: Proxy): void {
		let proxyTested = false;
		const customFetchFunc = async (req: any, signal: any) => {
			try {
				const gotInstance = gotWithProxy(proxy);

				const options: any = {
					method: req.method,
					headers: req.headers,
				};

				if (req.body) {
					options.body = Buffer.from(req.body);
				}

				if (!proxyTested) {
					proxyTested = true;
					await testProxy(gotInstance);
				}

				const gotResponse = await gotInstance(req.url, options);

				const getUrlResponse = {
					body: gotResponse.body
						? Uint8Array.from(Buffer.from(gotResponse.body))
						: null,
					headers: gotResponse.headers,
					statusCode: gotResponse.statusCode,
					statusMessage: gotResponse.statusMessage,
				};

				return getUrlResponse;
			} catch (err) {
				console.log(err);
			}
		};
		FetchRequest.registerGetUrl(customFetchFunc as any);
		for (let chain in this.#providers) {
			this.#providers[chain as ChainsType].provider = new JsonRpcProvider(
				config.RPCs[chain as ChainsType]
			);
		}
	}
}
