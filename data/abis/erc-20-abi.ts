export const ERC_20_ABI = [
	"function approve(address spender, uint256 amount)",
	"function balanceOf(address addr) view returns (uint)",
	"function allowance(address addr, address spender) view returns (uint)",
];
