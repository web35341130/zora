import { JsonRpcProvider } from "ethers";
// ALL CHAINS
export const Chains = {
	Ethereum: "polygon",
	Zora: "optimism",
} as const;
export type ChainsType = (typeof Chains)[keyof typeof Chains];
export type ChainsTypeKey = keyof typeof Chains;
export type ChainsData = {
	provider: JsonRpcProvider;
	explorer: string;
	chainId: number;
};
export type ChainsDataType = Record<ChainsType, ChainsData>;

// PROXY
export type Proxy = {
	ip: string;
	port: string;
	username: string;
	password: string;
};

export type Proxies = {
	[name: string]: Proxy;
};

// MISC
// export type WalletData = {
// 	name: number;
// 	address: string;
// 	signer: Wallet | HDNodeWallet;
// };

export type WalletData = {
	name: string;
	privateKey: string;
};

// MODULES
export const Modules = {
	BRIDGE: "BRIDGE",
	MINT: "MINT",
} as const;
export type ModulesType = (typeof Modules)[keyof typeof Modules];
