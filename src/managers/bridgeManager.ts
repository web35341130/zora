import { Contract, HDNodeWallet, Wallet, ethers } from "ethers";
import { config } from "../../config.js";
import { BRIDGE_ABI } from "../../data/abis/bridge-abi.js";
import { ProviderManager } from "../../data/chain-data.js";
import { Chains } from "../types.js";
import { asyncRetry } from "../utils/asyncRetry.js";
import { logger } from "../utils/logger.js";
import { randomFloat } from "../utils/utils.js";
import { estimateGas } from "../utils/web3.ts/estimateGas.js";
import { getTransactionState } from "../utils/web3.ts/getTransactionState.js";
import { handleInsufficientBalance } from "../utils/web3.ts/handleInsufficientBalance.js";
import { pollBalance } from "../utils/web3.ts/pollBalance.js";

export class BridgeManager {
	#walletAddress: string;
	#ethSigner: Wallet | HDNodeWallet;
	#bridgeContract: Contract;

	constructor(private wallet: Wallet | HDNodeWallet) {
		this.#walletAddress = this.wallet.address;
		this.#ethSigner = this.wallet.connect(ProviderManager.ethProvider);

		this.#bridgeContract = new ethers.Contract(
			"0x1a0ad011913A150f69f6A19DF447A0CfD9551054",
			BRIDGE_ABI,
			this.#ethSigner
		);
	}

	async bridge() {
		const operation = async () => {
			logger.info`Starting bridge...`;

			const to = this.#walletAddress;

			const value = ethers.parseEther(
				randomFloat(
					config.MODULES.BRIDGE.MINMAX_DEPOSIT_AMOUNT[0],
					config.MODULES.BRIDGE.MINMAX_DEPOSIT_AMOUNT[1],
					3
				).toString()
			);

			const gasLimit = 100000;
			const isCreation = false;
			const data = ethers.hexlify(new Uint8Array());
			const txArgs = [to, value, gasLimit, isCreation, data];

			const gas = await estimateGas(
				Chains.Ethereum,
				this.#bridgeContract,
				"depositTransaction",
				...txArgs,
				{ value }
			);

			const bridgeTx = await this.#bridgeContract.depositTransaction(
				...txArgs,
				{ ...gas, value }
			);

			await getTransactionState(bridgeTx, `bridging to ZORA`);

			await pollBalance({
				network: Chains.Zora,
				walletAddress: this.#walletAddress,
			});
		};

		await asyncRetry(operation, handleInsufficientBalance, [
			"bridge",
			this.#walletAddress,
			Chains.Ethereum,
		]);
	}
}
