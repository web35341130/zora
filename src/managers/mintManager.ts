import { HDNodeWallet, Wallet, ethers } from "ethers";
import { ERC_721_DROP_ABI } from "../../data/abis/erc-721-drop-abi.js";
import { ZORA_CREATOR_1155_ABI } from "../../data/abis/zora-creator-1155-abi.js";
import { ProviderManager } from "../../data/chain-data.js";
import { Chains } from "../types.js";
import { asyncRetry } from "../utils/asyncRetry.js";
import { logger } from "../utils/logger.js";
import { estimateGas } from "../utils/web3.ts/estimateGas.js";
import { getTransactionState } from "../utils/web3.ts/getTransactionState.js";
import { handleInsufficientBalance } from "../utils/web3.ts/handleInsufficientBalance.js";

export class MintManager {
	#walletAddress: string;
	#signer: Wallet | HDNodeWallet;

	constructor(private wallet: Wallet | HDNodeWallet) {
		this.#walletAddress = this.wallet.address;
		this.#signer = this.wallet.connect(ProviderManager.zoraProvider);
	}

	async #mint721(contractInfo: string) {
		const operation = async () => {
			logger.info`Starting mint721...`;

			const nftContract = new ethers.Contract(
				contractInfo,
				ERC_721_DROP_ABI,
				this.#signer
			);

			const quantity = 1;
			const value = ethers.parseEther("0.000777");

			const gas = await estimateGas(
				Chains.Zora,
				nftContract,
				"purchase",
				quantity,
				{ value }
			);

			const bridgeTx = await nftContract.purchase(quantity, {
				...gas,
				value,
			});

			await getTransactionState(bridgeTx, `minting 721 NFT`);
		};

		await asyncRetry(operation, handleInsufficientBalance, [
			"purchase",
			this.#walletAddress,
			Chains.Zora,
		]);
	}

	async #mint1155(contractInfo: [string, number]) {
		const operation = async () => {
			logger.info`Starting mint1155...`;

			const nftContract = new ethers.Contract(
				contractInfo[0],
				ZORA_CREATOR_1155_ABI,
				this.#signer
			);

			const minter = "0x169d9147dfc9409afa4e558df2c9abeebc020182";
			const tokenId = contractInfo[1];
			const quantity = 1;
			const minterArguments = ethers.hexlify(
				ethers.zeroPadValue(this.#walletAddress, 32)
			);
			const value = ethers.parseEther("0.000777");

			const txArgs = [minter, tokenId, quantity, minterArguments];

			const gas = await estimateGas(
				Chains.Zora,
				nftContract,
				"mint",
				...txArgs,
				{ value }
			);

			const bridgeTx = await nftContract.mint(...txArgs, {
				...gas,
				value,
			});

			await getTransactionState(bridgeTx, `minting 1155 NFT`);
		};

		await asyncRetry(operation, handleInsufficientBalance, [
			"mint",
			this.#walletAddress,
			Chains.Zora,
		]);
	}

	async mint(contractInfo: string | [string, number]) {
		console.log("ContractInfo", contractInfo);
		if (typeof contractInfo === "string") {
			await this.#mint721(contractInfo);
		} else {
			await this.#mint1155(contractInfo);
		}
	}
}
