import { HDNodeWallet, Wallet } from "ethers";
import { config } from "../config.js";
import { BridgeManager } from "./managers/bridgeManager.js";
import { MintManager } from "./managers/mintManager.js";
import { Modules, ModulesType } from "./types.js";
import { logger } from "./utils/logger.js";
import { shuffleArr } from "./utils/utils.js";

type Module = {
	fn: Function;
	args: any[];
	name: ModulesType;
};

const runBridgeManager = async (wallet: any) => {
	const bridge = new BridgeManager(wallet);
	await bridge.bridge();
};

const runMintManager = async (wallet: any) => {
	const mint = new MintManager(wallet);
	for (const contractInfo of config.MODULES.MINT.NFT_CONTRACTS) {
		await mint.mint(contractInfo as [string, number]);
	}
};

export async function runModules(wallet: Wallet | HDNodeWallet) {
	const modules: Module[] = [
		{
			fn: runBridgeManager,
			args: [wallet],
			name: Modules.BRIDGE,
		},
		{
			fn: runMintManager,
			args: [wallet],
			name: Modules.MINT,
		},
	];

	if (config.MODULES.ORDER === "random") {
		shuffleArr(modules);
	}

	for (const module of modules) {
		const moduleName = module.name;

		if (config.MODULES[moduleName].ENABLED) {
			try {
				await module.fn(...module.args);
				logger.log`Module ${moduleName} executed successfully.`;
			} catch (error) {
				logger.warn`Module ${moduleName} failed, switching to next one...`;
			}
		}
	}
}
