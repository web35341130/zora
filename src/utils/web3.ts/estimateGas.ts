import { Contract } from "ethers";
import { ChainsType } from "../../types.js";
import { logger } from "../logger.js";
import { ProviderManager } from "./../../../data/chain-data.js";

type GasFees = {
	maxPriorityFee: number;
	maxFee: number;
};

type GasInfoResponse = {
	safeLow: GasFees;
	standard: GasFees;
	fast: GasFees;
	estimatedBaseFee: number;
	blockTime: number;
	blockNumber: number;
};

type EstimateGasReturnProps = {
	gasLimit?: bigint;
	maxPriorityFeePerGas?: bigint;
	maxFeePerGas?: bigint;
	gasPrice?: bigint;
};

export async function estimateGas(
	chain: ChainsType,
	contract: Contract,
	methodName: string,
	...args: any[]
): Promise<EstimateGasReturnProps> {
	logger.info`Running estimateGas...`;
	try {
		const provider = ProviderManager.getProvider(chain);

		const block = await provider.getBlock("latest");
		const baseFee = block?.baseFeePerGas;
		const feeData = await provider.getFeeData();

		let gasPrice;
		let maxPriorityFeePerGas = feeData.maxPriorityFeePerGas || undefined;

		let maxFeePerGas;
		if (baseFee && maxPriorityFeePerGas)
			maxFeePerGas = baseFee + maxPriorityFeePerGas;

		let gasLimit;
		try {
			gasLimit = await contract[methodName].estimateGas(...args);
		} catch (err) {
			logger.error`Error while trying to estimate gasLimit: ${err}`;
			gasLimit = BigInt(350000);
		}

		return {
			...(gasLimit !== undefined && {
				gasLimit: gasLimit + BigInt(10000),
			}),
			...(maxPriorityFeePerGas !== undefined && { maxPriorityFeePerGas }),
			...(maxFeePerGas !== undefined && { maxFeePerGas }),
			...(gasPrice !== undefined && { gasPrice }),
		};
	} catch (err) {
		logger.error`Error in estimateGas: ${err}`;
		return {};
	}
}
