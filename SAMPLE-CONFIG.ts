import * as dotenv from "dotenv";
import path from "path";
import { fileURLToPath } from "url";
import { Chains, Modules } from "./src/types.js";

const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);

dotenv.config({ path: path.resolve(__dirname, "PATH TO YOUR WALLET FILE") });

if (!process.env.WALLETS) throw new Error("Missing wallet data");

export const config = {
	RPCs: {
		[Chains.Ethereum]: "https://eth.llamarpc.com",
		[Chains.Zora]: "https://rpc.zora.energy	",
	},
	EXCLUDED_WALLETS: [] as number[],
	SECRET_WALLET_DATA: JSON.parse(process.env.WALLETS),
	MODULES: {
		MINMAX_WALLET_WAIT_TIME: [60 * 3, 60 * 7], // seconds
		MINMAX_MODULES_WAIT_TIME: [60 * 3, 60 * 7], // seconds
		PROXY_ENFORCE: true, // true/false
		ORDER: "random",
		[Modules.BRIDGE]: {
			ENABLED: false, // true/false
			MINMAX_DEPOSIT_AMOUNT: [0.002, 0.002],
		},
		[Modules.MINT]: {
			ENABLED: true, // true/false
			NFT_CONTRACTS: [["0x5CA17551b686bAF0C6bd7727e153B95be9b1Ae0D", 1]], // for 721 only contract address: "address". for 1151 array ["address", tokenId]
		},
	},
};
